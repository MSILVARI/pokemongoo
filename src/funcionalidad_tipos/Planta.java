/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funcionalidad_tipos;

import funcionalidad.Ataque;
import pokemgopoo.PokemGoPOO;

/**
 *
 * @author milton
 */
public class Planta extends PokemGoPOO {
    
    
    public Planta(String nombre, int vida) {
		super(24, 31, nombre, vida);
		dañoBase = 13;
		defensa = 9;
		precision = 85;
	}

	
    @Override
	public int getAtaque(Ataque ataque) {
		int danioAtaque = 0;
		switch (ataque) {
		case HOJA_AFILADA:
			danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 9;
			break;
		case LATIGAZO:
			danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 4;
			break;
		case RAYO_SOLAR:
			danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 12;
			break;
		case FOGONAZO:
			danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90);
			break;
		case ENERGIBOLA:
			danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 8;
			break;
		case CUCHILLA_SOLAR:
			danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 6;
			break;
		case DANZA_PETALO:
			danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 5;
			break;
		default:
			danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 7;
			break;
		}
		setEnergia(getEnergia() - ataque.getEnergia());
		return danioAtaque;
	}

	
	public String getDefensa(PokemGoPOO enemigo, int ataqueEnemigo) {
		String cadena;
		int aleatorio = BatallaAleatoria.generarAleatorio(0, 100);

		if (aleatorio > precision) {
			return enemigo.getNombre() + " ha fallado el ataque";
		}
		if (enemigo.getClass() == Fuego.class || enemigo.getClass() == Volador.class) {
			setVida(getVida() - (ataqueEnemigo + defensa * 2));
			cadena = "ATAQUE CRITICO\nSe ha reducido en " + (ataqueEnemigo + defensa * 2) + " PS la salud de "
					+ getNombre();
		} else if (enemigo.getClass() == Planta.class || enemigo.getClass() == Electrico.class) {
			setVida(getVida() - (ataqueEnemigo - defensa));
			cadena = "Se ha reducido en " + (ataqueEnemigo - defensa) + " PS la salud de " + getNombre();
		} else {
			setVida(getVida() - (ataqueEnemigo - defensa * 2));
			cadena = "ES POCO EFECTIVO\nSe ha reducido en " + (ataqueEnemigo - defensa * 2) + " PS la salud de "
					+ getNombre();
		}

		if (getVida() <= 0) {
			cadena += "\n\t" + getNombre() + " se ha debilitado!";
		}

		return cadena;
	}


}
