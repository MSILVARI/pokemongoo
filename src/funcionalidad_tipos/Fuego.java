/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funcionalidad_tipos;

import funcionalidad.Ataque;
import pokemgopoo.PokemGoPOO;

/**
 *
 * @author milton
 */
public class Fuego extends PokemGoPOO {

    public Fuego(String nombre, int vida) {
        super(16, 23, nombre, vida);
        dañoBase = 16;
        defensa = 9;
        precision = 82;
    }

    @Override
    public int getAtaque(Ataque ataque) {
        int danioAtaque = 0;
        switch (ataque) {
            case ASCUAS:
                danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 6;
                break;
            case GIRO_FUEGO:
                danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 12;
                break;
            case LANZALLAMAS:
                danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 9;
                break;
            case LLAMARADA:
                danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 10;
                break;
            case ONDA_IGNEA:
                danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 11;
                break;
            case RUEDA_FUEGO:
                danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90);
                break;
            case HUMAREDA:
                danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 8;
                break;
            default:
                danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 7;
                break;
        }
        setEnergia(getEnergia() - ataque.getEnergia());
        return danioAtaque;
    }

    
    public String getDefensa(PokemGoPOO enemigo, int ataqueEnemigo) {
        String cadena;
        int aleatorio = BatallaAleatoria.generarAleatorio(0, 100);

        if (aleatorio > precision) {
            return enemigo.getNombre() + " ha fallado el ataque";
        }

        if (enemigo.getClass() == Agua.class) {
            setVida(getVida() - (ataqueEnemigo + defensa * 2));
            cadena = "ATAQUE CRITICO\nSe ha reducido en " + (ataqueEnemigo + defensa * 2) + " PS la salud de "
                    + getNombre();
        } else if (enemigo.getClass() == Fuego.class || enemigo.getClass() == Electrico.class
                || enemigo.getClass() == Volador.class) {
            setVida(getVida() - (ataqueEnemigo - defensa));
            cadena = "Se ha reducido en " + (ataqueEnemigo - defensa) + " PS la salud de " + getNombre();
        } else {
            setVida(getVida() - (ataqueEnemigo - defensa * 2));
            cadena = "ES POCO EFECTIVO\nSe ha reducido en " + (ataqueEnemigo - defensa * 2) + " PS la salud de "
                    + getNombre();
        }

        if (getVida() <= 0) {
            cadena += "\n\t" + getNombre() + " se ha debilitado!";
        }

        return cadena;

    }

}
