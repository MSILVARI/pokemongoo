/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funcionalidad_tipos;

import funcionalidad.Ataque;
import funcionalidad.EnergiaNoValidaException;

import pokemgopoo.PokemGoPOO;

/**
 *
 * @author milton
 */
public class Agua extends PokemGoPOO  {

    public Agua(String nombre, int vida)throws EnergiaNoValidaException {
        super(0, 7, nombre, vida);
        dañoBase = 15;
        defensa = 10;
        precision = 80;
    }

   
    @Override
    public int getAtaque(Ataque ataque) {
        int danoAtaque = 0;
        switch (ataque) {
            case PISTOLA_AGUA:
                danoAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 7;
                break;
            case HIDROBOMBA:
                danoAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 13;
                break;
            case RAYO_BURBUJA:
                danoAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 5;
                break;
            case SURF:
                danoAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 11;
                break;
            case HIDRO_CANON:
                danoAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 12;
                break;
            case ACUA_COLA:
                danoAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 8;
                break;
            case CASCADA:
                danoAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 10;
                break;
            default:
                danoAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 9;
                break;

        }
        setEnergia(getEnergia() - ataque.getEnergia());
        return danoAtaque;
    }

   
    
    public String getDefensa(PokemGoPOO enemigo, int ataqueEnemigo) {
        String cadena;
        int aleatorio = BatallaAleatoria.generarAleatorio(0, 100);

        if (aleatorio > precision) {
            return enemigo.getNombre() + " ha fallado el ataque";
        }

        if (enemigo.getClass() == Agua.class) {
            setVida(getVida() - (ataqueEnemigo - defensa));
            cadena = "Se ha reducido en " + (ataqueEnemigo - defensa) + " PS la salud de " + getNombre() + "\n"
                    + getNombre() + ": " + getVida() + "PS";
        } else if (enemigo.getClass() == Fuego.class) {
            setVida(getVida() - (ataqueEnemigo - defensa * 2));
            cadena = "ES POCO EFECTIVO\nSe ha reducido en " + (ataqueEnemigo - defensa * 2) + " PS la salud de "
                    + getNombre();
        } else if (enemigo.getClass() == Planta.class || enemigo.getClass() == Electrico.class) {
            setVida(getVida() - (ataqueEnemigo + defensa * 2));
            cadena = "ATAQUE CRITICO\nSe ha reducido en " + (ataqueEnemigo + defensa * 2) + " PS la salud de "
                    + getNombre();
        } else {
            setVida(getVida() - (ataqueEnemigo - defensa));
            cadena = "Se ha reducido en " + (ataqueEnemigo - defensa) + " PS la salud de " + getNombre();
        }

        if (getVida() <= 0) {
            cadena += "\n\t" + getNombre() + " se ha debilitado!";
        }

        return cadena;

    }

}
