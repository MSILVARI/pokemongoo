
package funcionalidad_tipos;

import funcionalidad.Ataque;
import java.io.Serializable;
import pokemgopoo.PokemGoPOO;

/**
 *
 * @author milton
 */
public class Electrico extends PokemGoPOO {

	

	public Electrico(String nombre, int vida){
		super(8, 15, nombre, vida);
		dañoBase = 14;
		defensa = 11;
		precision = 83;
	}


        @Override
	public int getAtaque(Ataque ataque)  {
		int danioAtaque = 0;
		switch (ataque) {
		case BOLA_VOLTIO:
			danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 7;
			break;
		case CHISPA:
			danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 5;
			break;
		case CHISPAZO:
			danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 6;
			break;
		case ELECTROCANION:
			danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 11;
			break;
		case IMPACTRUENO:
			danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 10;
			break;
		case ONDA_VOLTIO:
			danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 8;
			break;
		case RAYO:
			danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 12;
			break;
		default:
			danioAtaque = dañoBase + BatallaAleatoria.generarAleatorio(30, 90) + 9;
			break;
		}
		setEnergia(getEnergia() - ataque.getEnergia());
		return danioAtaque;
	}


	public String getDefensa(PokemGoPOO enemigo, int ataqueEnemigo) {
		String cadena;
		int aleatorio = BatallaAleatoria.generarAleatorio(0, 100);

		if (aleatorio > precision) {
			return enemigo.getNombre() + " ha fallado el ataque";
		}

		if (enemigo.getClass() == Agua.class || enemigo.getClass() == Volador.class) {
			setVida(getVida() - (ataqueEnemigo - defensa * 2));
			cadena = "ES POCO EFECTIVO\nSe ha reducido en " + (ataqueEnemigo - defensa * 2) + " PS la salud de "
					+ getNombre();
		} else if (enemigo.getClass() == Fuego.class || enemigo.getClass() == Electrico.class) {
			setVida(getVida() - (ataqueEnemigo - defensa));
			cadena = "Se ha reducido en " + (ataqueEnemigo - defensa) + " PS la salud de " + getNombre();
		} else {
			setVida(getVida() - (ataqueEnemigo + defensa * 2));
			cadena = "ATAQUE CRITICO\nSe ha reducido en " + (ataqueEnemigo + defensa * 2) + " PS la salud de "
					+ getNombre();
		}

		if (getVida() <= 0) {
			cadena += "\n\t" + getNombre() + " se ha debilitado!";
		}

		return cadena;

	}
}
