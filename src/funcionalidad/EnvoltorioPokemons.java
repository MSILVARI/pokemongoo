package funcionalidad;

import java.util.ArrayList;
import java.util.Iterator;
import pokemgopoo.PokemGoPOO;

public class EnvoltorioPokemons {

    private ArrayList<PokemGoPOO> lista = new ArrayList<PokemGoPOO>();
   

    public PokemGoPOO get(int index) throws PokemonNoExisteException {
        try {
            return lista.get(index);
        } catch (IndexOutOfBoundsException e) {
            throw new PokemonNoExisteException("El elemento no existe");
        }
    }

    public PokemGoPOO getElemento(Exception elemento) throws PokemonNoExisteException {
        try {
            return lista.get(obtenerIndice(elemento));
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new PokemonNoExisteException("El elemento no existe");
        }
    }

    public PokemGoPOO getPokemonNombre(String nombre) throws PokemonNoExisteException {
        PokemGoPOO pokemonEncontrado = null;
        for (PokemGoPOO pokemon : lista) {
            if (pokemon.getNombre().equalsIgnoreCase(nombre)) {
                pokemonEncontrado = pokemon;
            }
        }
        if (pokemonEncontrado != null) {
            return pokemonEncontrado;
        } else {
            throw new PokemonNoExisteException("El pokemon no existe");
        }
    }


    public int obtenerIndice(Exception elemento) {
        return lista.indexOf(elemento);
    }

    public int size() {
        return lista.size();
    }

    public void annadir(PokemGoPOO elemento) throws PokemonNoExisteException {
        if (lista.contains(elemento)) {
            throw new PokemonNoExisteException("Pokemons repetidos.");
        }

        lista.add(elemento);
    }

    public void eliminar(PokemGoPOO elemento) throws PokemonNoExisteException {
        if (!lista.remove(elemento)) {
            throw new PokemonNoExisteException("No existe el pokemon");
        }
    }


    public boolean isEmpty() {
        return lista.isEmpty();
    }

    public ArrayList<PokemGoPOO> getPokemonsTipo(Object tipo) throws PokemonNoExisteException {
        ArrayList<PokemGoPOO> pokemons = new ArrayList<PokemGoPOO>();
        for (PokemGoPOO pokemon : lista) {
            if (pokemon.getClass().getSimpleName().equals(tipo)) {
                pokemons.add(pokemon);
            }
        }
        if (pokemons.size() == 0) {
            throw new PokemonNoExisteException("No se ha encontrado ningún pokemon");
        }
        return pokemons;
    }

    public Iterator<PokemGoPOO> iterator() {
        return lista.iterator();
    }

    @Override
    public String toString() {
        StringBuilder cadena = new StringBuilder();
        for (PokemGoPOO pokemon : lista) {
            cadena.append(pokemon + "\n");
        }

        return cadena.toString();
    }

}
