package pokemgopoo;

import funcionalidad.Ataque;
import funcionalidad.EnergiaNoValidaException;
import funcionalidad_tipos.Agua;
import funcionalidad_tipos.Electrico;
import funcionalidad_tipos.Planta;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author milton
 */
public class Principal_Usu_Pokemo {

    public static void main(String[] args) {

        int op = 0;
        int i = 0;
        Scanner sc = new Scanner(System.in);
        do {
            try {
                System.out.println("---M E N U---");
                System.out.println("1) Ingesa datos");
                System.out.println("2) Atacar");
                System.out.println("3. Defensa");
                System.out.println("3) Salir");
                op = sc.nextInt();
                System.out.flush();
                Scanner lector = new Scanner(System.in);
                PokemGoPOO pokem = new PokemGoPOO() {
                    @Override
                    public int getAtaque(Ataque ataque) {
                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }
                };

                switch (op) {

                    case 1:
                        System.out.println("Ingrese nombre");
                        String nombre = lector.nextLine(); // aqui deberia guardar el dato
                        pokem.setNombre("milton");  // y aqui pasarselo al metodo set para despues ser mostrado, o no?

                        break;

                    case 2:

                        System.out.println("ATACAR CON AGUA");
                        Agua agua = new Agua(pokem.getNombre(), pokem.getVida());
                        System.out.println(" ATACAR   " + agua.getAtaque(Ataque.HIDROBOMBA));
                        System.out.println("ATACAR CON ELECTRICIDAD");
                        Electrico elc = new Electrico("PIKACHU", pokem.getVida());
                        System.out.println("RAYO   " + elc.getAtaque(Ataque.ELECTROCANION));
                        System.out.println("");
                        break;
                    case 3:
                        System.out.println("DEfENDER CON PLANTA");
                        Planta plt = new Planta(pokem.getNombre(), pokem.getVida());
                        pokem.setDefensa(20);
                        System.out.println("DEFENSA  " + plt.getDefensa());
                        System.out.println("");
                        break;
                }
            } catch (EnergiaNoValidaException ex) {
                Logger.getLogger(Principal_Usu_Pokemo.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (op <= 4);
    }
}
